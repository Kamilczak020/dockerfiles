# Dockerfiles

The following are dockerfiles, maintained for ease of deployment of third-party applications and tools.

### Currently available dockerfiles:

```
KUBERNETES

This image is meant for deploying applications on a kubernetes cluster, using gitlab runners.
It is outfitted with:
 - build-essential
 - ca-certificates
 - curl
 - dnsutils
 - git
 - gnupg2
 - jq
 - openssh-client
 - python-dev
 - ssh
 - telnet
 - unzip
 - wget
 - zip
 - kubectl
 - blackbox
 ```